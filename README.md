# Prosjekt 2 - IMT2291 - Våren 2018 #

# Prosjektdeltakere #
| Navn                | Studie | Studentnr. |
| --------------------|:------:|-----------:|
| [Brede Fritjof Klausen](https://bitbucket.org/Brede_F_Klausen/) | BPROG  | 473211 |
| [Johan Aanesen](https://bitbucket.org/Aanesen/)        | BPROG | 473182 |
| [Robin-Andrè Due Herrmann](https://bitbucket.org/raherrma/)      | BWU | 472510 |

Beskrivelsen til oppgaven finnes i [Wikien til det opprinnelige prosjektet](https://bitbucket.org/okolloen/imt2291-prosjekt2-v2018/wiki/Home).

Bruk Wikien til deres eget prosjekt til å dokumentere prosjektet slik det er beskrevet i oppgaven.

# Install & Req #
-----------------------------
* xampp
* php


* Clone prosjekt til xampp/htdocs
* Bytte DocumentRoot i 'Apache (httpd.conf)' i xampp til `DocumentRoot "C:/xampp/htdocs/imt-2291-prosjekt2-v2018"`
* bower install
* Importer SQL filen til mySql gjennom phpMyAdmin
* Prosjektet skal nå være tilgjengelig under URL: localhost
* Admin bruker: admin@metoobe.com pass: admin