<?php
require_once "classes/Urge.php";

$db     = Urge::requireDatabase();
$userid = User::getLoggedInUserid();

$isUser = false;

$user = null;
if ($userid) {
    $user = User::get($db, $userid);
    $isUser = true;
}


echo json_encode(array(
    'userid' => $userid, 
    'user' => $user,
    'isUser' => $isUser,
 ));
