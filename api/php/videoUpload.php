<?php
require_once "../classes/Urge.php";

$db                  = Urge::requireDatabase();
$userid              = Urge::requireLoggedInUser();
list($descr, $title, $fileThumbnail) = Urge::requireParameterArray('descr', 'videotitle', 'file-thumbnail');
$fileVideo           = Urge::getFileParameterOrNull('file-video');
//$fileThumbnail       = Urge::getFileParameterOrNull('file-thumbnail');
//$fileThumbnail = $_POST['file-thumbnail'];

// 0. Set default video
$tmp_filepath  = '';
$mimetypeVideo = '';


if ($fileVideo) {
    // 1. File format validation of video and thumbnail
    $tmp_filepath  = $fileVideo['tmp_name'];
    $mimetypeVideo = $fileVideo['type'];
    if ( $mimetypeVideo != 'video/mp4'  && $mimetypeVideo != 'video/webm' && $mimetypeVideo != 'video/ogg') {
        Urge::gotoError(400, "Bad request, file format has to be [mp4|webm|ogg]");
    }
}

//$scaledThumbnail = $fileThumbnail;
$fileThumbnail = str_replace('data:image/png;base64,', '', $fileThumbnail);
$fileThumbnail = str_replace(' ', '+', $fileThumbnail);
$fileThumbnail = base64_decode($fileThumbnail);

/*
if ($fileThumbnail) {

    // 2. Scale thumbnail to uniform size
    $thumbnail = file_get_contents($fileThumbnail['tmp_name']);
    $scaledThumbnail = Urge::scaleThumbnail($thumbnail);
}*/


// 3. Add new video entry to the database4.
$videoid = Video::add($db, $userid, $title, $descr, $mimetypeVideo, $fileThumbnail);
if (!$videoid) {
    Urge::gotoError(500, 'Server did not manage to upload video');
}

if ($fileVideo) {
    // 4. Save video file to storage
    $result = Video::saveToFile($userid, $videoid, $tmp_filepath, $mimetypeVideo);
    if (!$result) {
        Video::delete($db, $videoid);
        Urge::gotoError(500,'ERROR - $result = Video::saveToFile() - Unable to move uploaded file to destination folder.');
    }
}


Urge::gotoVideo($videoid);

