<?php
require_once "../classes/Urge.php";

$db      = Urge::requireDatabase();
$userid  = User::getLoggedInUserid();

list($videoID, $videoTitle, $videoDesc, $videoOwner) = Urge::requireParameterArray(
    'video-id','video-title', 'video-description', 'video-owner');

$videoSubs = Urge::getFileParameterOrNull('video-subs');



if ($userid == $videoOwner){

    if(isset($videoSubs)){
        Video::saveSubsToFile($userid, $videoID, $videoSubs['tmp_name']);
        Urge::gotoVideo($videoID);
    }

    if(Video::updateVideoTitleDescription($db, $videoID, $videoTitle, $videoDesc)){
        Urge::gotoVideo($videoID);
    }
}else{
    Urge::gotoError(400, "Something went wrong updating the video.".$userid.$videoOwner);
}