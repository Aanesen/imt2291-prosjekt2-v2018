<?php
require_once "../classes/Urge.php";

$db = Urge::requireDatabase();
$userid = User::getLoggedInUserid();

User::delete($db, $userid);

header("Location: /php/userLogout.php");