<?php
require_once "classes/Urge.php";

$db     = Urge::requireDatabase();
$userid = User::getLoggedInUserid();
$subscribed = false;

if (!isset($_GET['id']) && !$userid) {
    Urge::gotoLogin();
}

if (!isset($_GET['id']) && $userid) {   
    $user = User::get($db, $userid);
    echo json_encode(array(
        'title' => 'home',
        'userid' => $userid,
        'user' => $user,        
        'createMode' => true,
    ));     
    exit();
}

$playlistID = $_GET['id'];
$playlist   = Playlist::get($db, $playlistID);
$videos     = Playlist::getVideos($db, $playlistID, true);

// Encode thumbnails
$playlist = Urge::encodeThumbnailToBase64($playlist);
$videos   = Urge::encodeThumbnailsToBase64($videos);


if (!$userid) {
    echo json_encode(array(
        'title' => 'home',
        'playlist' => $playlist,
        'videos' => $videos,
    ));
    exit();
}

$editMode = false;
if ($playlist['userid'] === $userid) {
    $editMode = true;
}

$subscribed = Playlist::checkIfSubscribed($db, $userid, $playlistID);

header("Content-Type: application/json; charset=utf-8");

$user = User::get($db, $userid);
echo json_encode(array(
    'title' => 'home',
    'userid' => $userid,
    'user' => $user,
    'editMode' => $editMode,
    'playlist' => $playlist,
    'videos' => $videos,
    'subscribed' => $subscribed,
    'createMode' => false,
));