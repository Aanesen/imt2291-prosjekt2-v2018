<?php
require_once "classes/Urge.php";


if (User::getLoggedInUserid()) {
    Urge::gotoHome();
}

$hasEmail = (isset($_COOKIE['email']));
$email = $hasEmail ? $_COOKIE['email'] : "";

echo json_encode(array('email' => $email, 'hasEmail' => $hasEmail, ));
